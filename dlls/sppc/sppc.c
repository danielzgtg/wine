/*
 *
 * Copyright 2008 Alistair Leslie-Hughes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>

#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windef.h"
#include "winbase.h"
#include "winternl.h"
#include "wine/debug.h"

#include "slpublic.h"
#include "slerror.h"

WINE_DEFAULT_DEBUG_CHANNEL(slc);

HRESULT WINAPI SLGetLicensingStatusInformation(HSLC handle, const SLID *app, const SLID *product,
                                               LPCWSTR name, UINT *count, SL_LICENSING_STATUS **status)
{
    FIXME("(%p %p %p %s %p %p) stub\n", handle, app, product, debugstr_w(name), count, status );

    return SL_E_RIGHT_NOT_CONSUMED;
}

HRESULT WINAPI SLGetApplicationPolicy(HSLP handle, const WCHAR *name, UINT *type, UINT *count, BYTE **data)
{
    FIXME("(%p %s %p %p %p) stub\n", handle, debugstr_w(name), type, count, data );

    *count = 0;
    *data = NULL;

    return S_OK;
}

HRESULT WINAPI SLGetSLIDList(HSLC handle, UINT queryType, const SLID *query, UINT returnType, UINT *count, SLID **data)
{
    FIXME("(%p %u %p %u %p %p) stub\n", handle, queryType, query, returnType, count, data);

    *count = 0;
    *data = (SLID *)0xdeadbeef;

    return S_OK;
}

HRESULT WINAPI SLInstallLicense(HSLC handle, UINT count, const BYTE *data, SLID *file)
{
    UINT i;
    FIXME("(%p %u %p %p) stub\n", handle, count, data, file);

    for (i = 0; i < count; i++) {
        memset(&file[i], 0, sizeof(SLID));
    }

    return S_OK;
}

HRESULT WINAPI SLOpen(HSLC *handle)
{
    FIXME("(%p) stub\n", handle );

    if (!handle)
        return E_INVALIDARG;

    *handle = (HSLC)0xdeadbeef;

    /*
     * Microsoft office stops crashing and just adds to the title a suffix of
     * "(Non-Commercial Use) (Unlicensed Product)" if we make the thread sleep forever
     */
    for (;;) {
        Sleep(1000);
    }

    return S_OK;
}

HRESULT WINAPI SLClose(HSLC handle)
{
    FIXME("(%p) stub\n", handle );

    return S_OK;
}

HRESULT WINAPI SLLoadApplicationPolicies(const SLID *app, const SLID *product, DWORD flags, HSLP *result)
{
    FIXME("(%s,%s,%lx,%p) stub\n", wine_dbgstr_guid(app), wine_dbgstr_guid(product), flags, result);

    *result = NULL;

    return S_OK;
}

HRESULT WINAPI SLPersistApplicationPolicies(const SLID *app, const SLID *product, DWORD flags)
{
    FIXME("(%s,%s,%lx) stub\n", wine_dbgstr_guid(app), wine_dbgstr_guid(product), flags);

    if (!app)
        return E_INVALIDARG;

    return S_OK;
}
